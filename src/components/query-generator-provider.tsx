import { FC, useCallback, useEffect, useState } from "react";

import { QueryGeneratorContext } from "../context";
import { QueryGenerator, GqlQueryHandler } from "../query-generator";

export interface QueryGeneratorProviderProps {
	queryHandler: GqlQueryHandler
}

export const QueryGeneratorProvider: FC<QueryGeneratorProviderProps> = (props) => {
	const {queryHandler} = props;

	const [queryGenerator, setQueryGenerator] = useState<QueryGenerator>();

	const initQueryGenerator = useCallback(async () => {
		const queryGenerator = new QueryGenerator(queryHandler);
		await queryGenerator.init();

		setQueryGenerator(queryGenerator);
	}, []);

	useEffect(() => {
		initQueryGenerator();
	}, [initQueryGenerator]);

	if (!queryGenerator) {
		return null;
	}

	return (
		<QueryGeneratorContext.Provider value={queryGenerator}>
			{props.children}
		</QueryGeneratorContext.Provider>
	)
}
