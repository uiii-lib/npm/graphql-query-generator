import { createContext } from "react";

import { QueryGenerator } from "./query-generator";

export const QueryGeneratorContext = createContext<QueryGenerator|undefined>(undefined);
