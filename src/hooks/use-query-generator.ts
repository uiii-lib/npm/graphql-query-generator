import { useContext } from 'react';

import { QueryGeneratorContext } from '../context';

export const useQueryGenerator = (type: string, name: string, alias?: string) => {
	const queryGenerator = useContext(QueryGeneratorContext)!;
	return queryGenerator.generateQuery(type, name, alias);
}
