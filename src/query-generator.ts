import { buildClientSchema, DocumentNode, getIntrospectionQuery, GraphQLField, GraphQLObjectType, GraphQLSchema } from 'graphql';

import { upperFirst } from 'lodash';

export type GqlQueryHandler<Data = any, Variables extends object = {}> = (query: string|DocumentNode, variables?: Variables) => Promise<Data>;

export class QueryGenerator {
	schema?: GraphQLSchema;

	constructor(private queryGql: GqlQueryHandler) {}

	async init() {
		this.schema = await this.getSchema();
		console.log(this.schema);
	}

	generateQuery(type: string, name: string, alias?: string) {
		console.log("GQ", type, name, alias);
		const typeSchema = (this.schema as any)[`get${upperFirst(type)}Type`]() as GraphQLObjectType;

		console.log("TS", typeSchema);
		const field = typeSchema.getFields()[name]!;

		if (!field) {
			return "";
		}

		const innerQuery = this.generateInnerQuery(this.schema!, field, 1, alias);

		const variablesGql = innerQuery.variables.length > 0
			? ` (${innerQuery.variables.map(v => `$${v.name}: ${v.type}`).join(', ')}) `
			: "";

		return `${type}${variablesGql}{\n${innerQuery.gql}\n}`;
	}

	private generateInnerQuery(schema: GraphQLSchema, field: GraphQLField<any, any>, indent: number, alias?: string): {variables: {name: string, type: string}[], gql: string} {
		let argsGql = field.args.length > 0
			? `(${field.args.map(a => `${a.name}: $${a.name}`).join(', ')})`
			: "";

		let variables = field.args.map(f => ({
			name: f.name,
			type: f.type.toString()
		}));

		let fieldTypeName = field.type.toString().replace(/[\[\]!]/g, '');
		let fieldType = schema.getType(fieldTypeName)!;

		let subQueries = ('getFields' in fieldType)
			? Object.values(fieldType.getFields()).map(f => this.generateInnerQuery(schema, f, indent + 1))
			: [];

		subQueries.forEach(sq => variables.push(...sq.variables));

		let fieldsGql = subQueries.length > 0
			? ` {\n${subQueries.map(sq => sq.gql).join('\n')}\n${'\t'.repeat(indent)}}`
			: "";

		const aliasGql = alias ? `${alias}: ` : "";

		return {
			variables,
			gql: `${'\t'.repeat(indent)}${aliasGql}${field.name}${argsGql}${fieldsGql}`
		};
	}

	private async getSchema() {
		const data = await this.queryGql(getIntrospectionQuery());
		return buildClientSchema(data);
	}
}
